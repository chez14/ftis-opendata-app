import {action, observable, decorate} from 'mobx';

import axios from "axios";
class UserStore {
    kuliahData = [];
    isKuliahDataFetching = true;

    fetchKuliahData() {
        this.isKuliahDataFetching = true;
        return axios.get("https://ftisunpar.github.io/data/prasyarat.json").then(data=>{
            this.kuliahData = data.data;
            this.isKuliahDataFetching = false;
            return Promise.resolve();
        })
    }
}

decorate(UserStore, {
    kuliahData: observable,
    isKuliahDataFetching: observable,
    fetchKuliahData: action
    
})


export default UserStore;