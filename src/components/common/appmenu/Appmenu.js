import React, { useState } from 'react';

import AppBar from '@material-ui/core/AppBar';
import Hidden from '@material-ui/core/Hidden';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Container from '@material-ui/core/Container';
import { fade, makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import ShareIcon from '@material-ui/icons/Share';
import InfoIcon from '@material-ui/icons/Info';
import BugReportIcon from '@material-ui/icons/BugReport';
import Appdrawer from "./Appdrawer";
import InputBase from '@material-ui/core/InputBase';
import { observer, inject } from "mobx-react";
import { useQueryParam, StringParam } from 'use-query-params';
import Badge from '@material-ui/core/Badge';
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  toolbar: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  }
}));


const Appmenu = () => {
  const classes = useStyles();
  const [query, setQuery] = useQueryParam('q', StringParam);
  const [preQuery, setPreQuery] = useState("");
  const [isDrawerOpen, setDrawerOpen] = useState(false);
  let handleDrawer = () => {
    setDrawerOpen(!isDrawerOpen);
  }
  function submit(e) {
    e.preventDefault();
    setQuery(preQuery);
  }
  return (<>
    <nav className={classes.root}>
      <AppBar position="static">
        <Container>
          <Toolbar className={classes.toolbar}>
            <Hidden smUp implementation="css">
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawer}>
                <MenuIcon />
              </IconButton>
            </Hidden>

            <div className={classes.title}>
              <Badge anchorOrigin={{ vertical: "bottom", horizontal: "right" }} badgeContent="v2" overlap="rectangle" color="secondary">
                <Typography variant="h5" noWrap>
                  Pho
                </Typography>
              </Badge>
            </div>
            <form onSubmit={submit}>
              <InputBase
                onChange={(e) => setPreQuery(e.target.value)}
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                defaultValue={query}
                inputProps={{ 'aria-label': 'search' }}
              />
            </form>
            <IconButton color="inherit" onClick={submit}>
              <SearchIcon />
            </IconButton>
          </Toolbar>
        </Container>
      </AppBar>
    </nav>
    <Appdrawer onRequestDrawerClose={handleDrawer} openDrawer={isDrawerOpen}>
      <List>
        <ListItem component={Link} to="/" button key={"home"}>
          <ListItemIcon><HomeIcon /></ListItemIcon>
          <ListItemText>Beranda</ListItemText>
        </ListItem>
        <ListItem component={Link} to="/graph" button key={"graph"}>
          <ListItemIcon><ShareIcon /></ListItemIcon>
          <ListItemText>Graph</ListItemText>
        </ListItem>
        <ListItem component={Link} to="about" button key={"about"}>
          <ListItemIcon><InfoIcon /></ListItemIcon>
          <ListItemText>Tentang</ListItemText>
        </ListItem>
        <ListItem component={"a"} href="https://gitlab.com/chez14/ftis-opendata-app" button key={"gitlab"}>
          <ListItemIcon><BugReportIcon /></ListItemIcon>
          <ListItemText>See Repository</ListItemText>
        </ListItem>
      </List>
    </Appdrawer>
  </>)
}
Appmenu.propTypes = {
}

export default inject("userStore")(observer(Appmenu));