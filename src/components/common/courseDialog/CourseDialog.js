import React from 'react';
import { inject, observer } from 'mobx-react';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { When, Then, If, Else } from "react-if";
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import CourseGraph from '../coursegraph/CourseGraph';
import CourseCard from '../coursecard/Coursecard';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import lime from '@material-ui/core/colors/lime';

const useStyles = makeStyles(theme => ({
  dialogWrapper: {
    padding: (theme.spacing(4) + "px 24px")
  },
  chippers: {
    marginRight: theme.spacing(0.5),
    marginBottom: theme.spacing(0.5)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  headerWrapper: {
    backgroundColor: lime[300],
    padding: theme.spacing(2),
    borderRadius: theme.shape.borderRadius
  }
}));

function PaperComponent(props) {
  return (
    <Draggable cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

const CourseDialog = ({ course, userStore, open, handleClose }) => {
  const classes = useStyles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  let prasyaratLulus = [];
  let prasyaratTempuh = [];
  let prasyaratBersama = [];
  if (userStore && userStore.kuliahData) {
    prasyaratLulus = userStore.kuliahData.filter(k => course.prasyarat.lulus.includes(k.kode));
  }
  if (userStore && userStore.kuliahData) {
    prasyaratTempuh = userStore.kuliahData.filter(k => course.prasyarat.tempuh.includes(k.kode));
  }
  if (userStore && userStore.kuliahData) {
    prasyaratBersama = userStore.kuliahData.filter(k => course.prasyarat.bersamaan.includes(k.kode));
  }

  let prasya = new Set();
  userStore.kuliahData.filter(k => k.prasyarat.bersamaan.includes(course.kode) || k.prasyarat.tempuh.includes(course.kode) || k.prasyarat.lulus.includes(course.kode))
    .map(d => prasya.add(d.kode));
  prasya.add(course.kode);
  let courseGraphData = userStore.kuliahData.filter(e => prasya.has(e.kode));
  
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      PaperComponent={PaperComponent}
      maxWidth="lg"
      fullWidth
      scroll="body"
      fullScreen={fullScreen}
    >
      <DialogTitle style={{ cursor: 'move' }}>
        Detil Mata Kuliah

        <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <Fade in={open}>
        <DialogContent className={classes.dialogWrapper}>
          <Grid container spacing={3}>
            <Grid item xs={12} lg={6}>
              <Box className={classes.headerWrapper}>
                <Typography variant="h5" gutterBottom>{course.nama} ({course.kode})</Typography>
                <Chip label={"Semester " + course.semester} color="primary" className={classes.chippers} />
                <Chip label="Sks" variant="outlined" avatar={<Avatar>{course.sks}</Avatar>} className={classes.chippers} />
                <When condition={course.wajib}>
                  <Chip label={<b>WAJIB</b>} avatar={<Avatar><ErrorOutlineIcon /></Avatar>} color="secondary" className={classes.chippers} />
                </When>
              </Box>

              <Box mt={2}>
                <Typography variant="h5" gutterBottom>Prasyarat</Typography>
                <Box mt={1}>
                  <Typography variant="h6">Prasyarat Lulus</Typography>
                  <Typography variant="body1">
                    <span style={{ color: "blue" }}>Garis biru</span><br />
                    Anda harus lulus (>E) mata kuliah berikut untuk dapat mengambil MK ini
                  </Typography>
                  <If condition={prasyaratLulus.length > 0}>
                    <Then>
                      <Grid container spacing={3}>
                        {prasyaratLulus.map(function (data, i) {
                          return <Grid item xs={6} key={i}>
                            <CourseCard data={data} />
                          </Grid>
                        })}
                      </Grid>
                    </Then>
                    <Else>
                      <Typography color="textSecondary" paragraph><i>Tidak ada prasyarat lulus.</i></Typography>
                    </Else>
                  </If>
                </Box>

                <Box mt={1}>
                  <Typography variant="h6">Prasyarat Tempuh</Typography>
                  <Typography variant="body1">
                    <span style={{ color: "black" }}>Garis hitam</span><br/>
                    Anda harus menempuh mata kuliah berikut untuk dapat mengambil MK ini
                  </Typography>
                  <If condition={prasyaratTempuh.length > 0}>
                    <Then>
                      <Grid container spacing={3}>
                        {prasyaratTempuh.map(function (data, i) {
                          return <Grid item xs={6} key={i}>
                            <CourseCard data={data} />
                          </Grid>
                        })}
                      </Grid>
                    </Then>
                    <Else>
                      <Typography color="textSecondary" paragraph><i>Tidak ada prasyarat tempuh.</i></Typography>
                    </Else>
                  </If>
                </Box>

                <Box mt={1}>
                  <Typography variant="h6">Prasyarat Ditempuh Bersama</Typography>
                  <Typography variant="body1">
                    <span style={{ color: "red" }}>Garis putus merah</span><br/>
                    Mata kuliah ini harus diambil bersamaan dengan matakuliah berikut:
                  </Typography>
                  <If condition={prasyaratBersama.length > 0}>
                    <Then>
                      <Grid container spacing={3}>
                        {prasyaratBersama.map(function (data, i) {
                          return <Grid item xs={6} key={i}>
                            <CourseCard data={data} />
                          </Grid>
                        })}
                      </Grid>
                    </Then>
                    <Else>
                      <Typography color="textSecondary" paragraph><i>Tidak ada prasyarat bersamaan apapun.</i></Typography>
                    </Else>
                  </If>
                </Box>

                <Box mt={1}>
                  <Typography variant="h6">Berlaku untuk angkatan</Typography>

                  <If condition={!!course.prasyarat.berlakuAngkatan}>
                    <Then>
                      <Typography variant="body1">
                        Mata kuliah ini mulai berlaku sejak angkatan {course.prasyarat.berlakuAngkatan}
                      </Typography>
                    </Then>
                    <Else>
                      <Typography color="textSecondary" paragraph><i>Tidak ada berlaku angkatan.</i></Typography>
                    </Else>
                  </If>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} lg={6}>
              <Box display="block">
                <Typography variant="h5">Course Graph</Typography>
              </Box>
              <When condition={userStore && userStore.kuliahData && userStore.kuliahData.length > 0}>
                <CourseGraph all={userStore.kuliahData} selected={courseGraphData} hierarchical height="100%" specials={[course]} />
              </When>
            </Grid>
          </Grid>
        </DialogContent>
      </Fade>
    </Dialog>
  )
}
CourseDialog.propTypes = {
}

export default inject("userStore")(observer(CourseDialog));