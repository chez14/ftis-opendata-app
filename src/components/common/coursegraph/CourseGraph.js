import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Graph from "react-graph-vis";
import CourseDialog from "../courseDialog/CourseDialog";
const baseEdgeStyle = {
  font: {
    color: '#343434',
    size: 14, // px
    face: 'Roboto',
    background: 'none',
    strokeWidth: 2, // px
    strokeColor: '#ffffff',
  },
  smooth: {
    enabled: true,
    type: "cubicBezier",
    roundness: 0.3
  },
  selfReferenceSize: 8
}

const edgeStyles = {
  lulus: {
    ...baseEdgeStyle,
    arrows: "to",
    width: 2,
    color: "blue"
  },
  tempuh: {
    ...baseEdgeStyle,
    arrows: "to"
  },
  bersamaan: {
    ...baseEdgeStyle,
    arrows: null,
    width: 2,
    color: "red",
    dashes: [10, 10]
  }
}

class CourseGraph extends Component {
  state = {
    buka: [],
    network: null,
    readied: false
  }

  static get propTypes() {
    return {
      all: PropTypes.arrayOf(PropTypes.object).isRequired,
      selected: PropTypes.arrayOf(PropTypes.object),
      hierarchical: PropTypes.bool,
      edgeColor: PropTypes.string,
      height: PropTypes.string,
      autoresize: PropTypes.bool,
      specials: PropTypes.array,
      onNetworkReady: PropTypes.func
    }

  }

  static get defaultProps() {
    return {
      selected: [],
      all: [],
      hierarchical: false,
      edgeColor: "#000000",
      height: "700px",
      autoresize: false,
      specials: [],
      onNetworkReady: () => { }
    }
  }

  getDependencies(mks) {
    const { all } = this.props;
    let dependants = new Set();

    mks.forEach((mk) => {
      if (mk.prasyarat.tempuh) {
        mk.prasyarat.tempuh.map(e=>dependants.add(e))
      }
      if (mk.prasyarat.lulus) {
        mk.prasyarat.lulus.map(e=>dependants.add(e))
      }
      if (mk.prasyarat.bersamaan) {
        mk.prasyarat.bersamaan.map(e=>dependants.add(e))
      }
    })

    return all.filter(matakuliah => dependants.has(matakuliah.kode));
  }

  bukaPopup(nodeData) {
    const { nodes } = nodeData;
    const { all } = this.props;

    this.setState({
      buka: all.filter(e => nodes.includes(e.kode))
    })
  }

  render() {
    const { all, selected, hierarchical, edgeColor, height, autoresize, specials, onNetworkReady } = this.props;
    const coursePopup = (this.state.buka || []);

    const options = {
      layout: {
        hierarchical: {
          enabled: hierarchical,
          nodeSpacing: 50
        }
      },
      edges: {
        color: edgeColor,
      },
      height: height,
      physics: {
        enabled: true,
        hierarchicalRepulsion: {
          centralGravity: 0.0,
          springLength: 100,
          springConstant: 0.01,
          nodeDistance: 200,
          damping: 0.09
        },
        solver: 'hierarchicalRepulsion'
      },
      autoResize: autoresize
    };

    let nodes = all;
    let edgesSource = [];
    if (selected && selected.length > 0) {
      nodes = [];
      nodes.push(...selected);
      nodes.push(...this.getDependencies(selected));

      edgesSource = selected;
    } else {
      edgesSource = all;
    }

    // nodes preproccessing
    let nMentah = new Set();
    let nod = nodes;
    nodes.map(e => nMentah.add(e.kode));
    nodes = all.filter(e => nMentah.has(e.kode));
    console.log("Nodes ", nod, nodes, Array.from(nMentah), selected);

    let specials_node = new Set();
    specials.map(e => specials_node.add(e.kode));

    let edge = edgesSource.map((course) => {
      let tempuh = course.prasyarat.tempuh.map(prasyarat => ({
        ...edgeStyles.tempuh,
        from: prasyarat,
        to: course.kode
      }));
      let lulus = course.prasyarat.lulus.map(prasyarat => ({
        ...edgeStyles.lulus,
        from: prasyarat,
        to: course.kode
      }));
      let bersamaan = course.prasyarat.bersamaan.map(prasyarat => ({
        ...edgeStyles.bersamaan,
        from: prasyarat,
        to: course.kode
      }));
      return [...tempuh, ...lulus, ...bersamaan];
    }).flat();


    let graph = {
      nodes: nodes.map((e) => {
        let baseStyle = {
          id: e.kode,
          label: e.kode + "\n" + e.nama,
          shape: "box",
          level: e.semester,
          shapeProperties: {
            borderDashes: false, // only for borders
            borderRadius: 0,     // only for box shape
            interpolation: false,  // only for image and circularImage shapes
            useImageSize: false,  // only for image and circularImage shapes
            useBorderWithImage: false  // only for image shape
          },
          borderWidth: 0,
          borderWidthSelected: 0,
        }

        if (specials_node.has(e.kode)) {
          baseStyle.color = { background: "orange" };
        }
        return baseStyle;
      }),
      edges: edge
    };

    return (
      <>
        <Graph
          graph={graph}
          options={options}
          events={{
            doubleClick: e => this.bukaPopup(e),
            stabilized: e => {
                onNetworkReady(this.state.network)
            }
          }}
          getNetwork={network => {
            this.setState({ network: network });
          }}
        />
        {coursePopup.map((e, i) =>
          <CourseDialog course={e} open handleClose={() => this.setState({ buka: [] })} key={i} />
        )}
      </>
    )
  }
}
export default (CourseGraph);