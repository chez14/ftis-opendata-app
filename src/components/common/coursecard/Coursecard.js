import React from 'react';
import { inject, observer } from 'mobx-react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

import CourseDialog from "../courseDialog/CourseDialog";
import Badge from '@material-ui/core/Badge';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { When } from 'react-if';

const useStyles = makeStyles(theme => ({
  courseTitle: {
    minHeight: (1.5 * 4) + "rem",
    userSelect: "text"
  },
  courseCode: {
    userSelect: "text"
  },
  details_icon: {
    marginRight: theme.spacing(1)
  },
  chippers: {
    marginRight: theme.spacing(0.5),
    marginBottom: theme.spacing(0.5)
  },
  growableContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  growingChipper: {
    flexGrow: 1
  }
}));

const Coursecard = ({ data, userStore }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  let prasyarat = data.prasyarat.tempuh.length + data.prasyarat.lulus.length;
  if (data.prasyarat.berlakuAngkatan) {
    prasyarat += 1;
  }

  return (<>
    <Card className={classes.card}>
      <CardContent>
        <div className={classes.growableContainer}>
          <Box className={classes.growingChipper}>
            <Typography display="block" className={classes.courseCode}>{data.kode}</Typography>
          </Box>
          <When condition={data.wajib}>
            <Box>
              <Chip label={<b>WAJIB</b>} avatar={<Avatar><ErrorOutlineIcon /></Avatar>} color="secondary" className={classes.chippers} />
            </Box>
          </When>
        </div>
        <Typography variant="h5" className={classes.courseTitle} display="block">{data.nama}</Typography>
        <div>
          <Chip label={"Semester " + data.semester} color="primary" className={classes.chippers} />
          <Chip label="Sks" variant="outlined" avatar={<Avatar>{data.sks}</Avatar>} className={classes.chippers} />
         
        </div>
      </CardContent>

      <CardActions>
        <Badge badgeContent={prasyarat} color="secondary" anchorOrigin={{ vertical: "top", horizontal: "right" }}>
          <Button color="primary" onClick={handleOpen}> <AssignmentIcon className={classes.details_icon} /> Detil </Button>
        </Badge>
        <CourseDialog course={data} open={open} handleClose={handleClose} />
      </CardActions>
    </Card>
  </>
  )
}
Coursecard.propTypes = {
}

export default inject("userStore")(observer(Coursecard));