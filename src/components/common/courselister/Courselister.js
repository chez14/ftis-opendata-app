import React, {} from 'react';
import Box from '@material-ui/core/Box'
import CourseCard from '../coursecard/Coursecard';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ palette }) => {
  console.log(palette);
  return ({
    bodyBg: {
      background: palette.grey[300]
    }
  })
};

const Courselister = ({ data, userStore }) => {
  return (
    <Box mt={3}>
      <Grid container spacing={3}>
        {data.map(function (data, i) {
          return <Grid item xs={12} md={4} lg={3}  key={i}>
            <CourseCard data={data} key={i} />
          </Grid>
        })}
      </Grid>
    </Box>
  )
}
Courselister.propTypes = {
}

export default (withStyles(styles)(Courselister));