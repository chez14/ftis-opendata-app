import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Container } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Hidden from '@material-ui/core/Hidden';
import Link from '@material-ui/core/Link';
import { Helmet } from "react-helmet";

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  }, avatar: {
    margin: 10,
    width: 80,
    height: 80,
  },
  title: {
    marginTop: 50,
  }
}));

const contributors = [
  {
    alias: "Chris",
    fullName: "Gunawan Christianto",
    npm: "2016730011",
    link: "https://gitlab.com/chez14",
    profilePict: "https://avatars0.githubusercontent.com/u/5555026?s=460&v=4"
  },
  {
    alias: "Joseph",
    fullName: "Joseph Febriano Nathan",
    npm: "2016730007",
    link: "https://github.com/pilowman",
    profilePict: "https://avatars3.githubusercontent.com/u/32382830?s=460&v=4"
  },
  {
    alias: "Gerry",
    fullName: "Gerry Fernando Lesmana",
    npm: "2016730050",
    link: "https://github.com/faithjr",
    profilePict: "https://avatars1.githubusercontent.com/u/32383407?s=460&v=4"
  },
]


export default function AlignItemsList() {
  const classes = useStyles();

  return (
    <Box my={5}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Tentang</title>
      </Helmet>
      <Container>
        <Grid item xs={12} >
          <Typography variant="h4" align="center">
            Tentang Pho
           </Typography>
          <br />
          <Typography variant="body1" align="justify">
            Melihat Prasyarat dari suatu mata kuliah terkadang sulit karena suatu mata kuliah
            bisa memiliki banyak prasyarat dan prasyaratnya juga memiliki prasyarat lagi.
            Kami berharap aplikasi Pho ini dapat membantu mahasiswa-mahasiwa FTIS UNPAR dalam
            memperlihatkan prasyarat dari suatu mata kuliah yang ingin mereka ambil dalam bentuk list dan tree
            untuk semester yang akan datang.
      </Typography>
          <br />
          <Typography align="justify">
            Data yang digunakan pada aplikasi ini seluruhnya diambil dari FTIS UNPAR, Jika menemukan
            masalah anda dapat langsung membuat issue ke repository{' '}
            <Link href="https://github.com/ftisunpar/data/">
              FTIS UNPAR Open Data
            </Link>
          </Typography>

          <Typography variant="h6" className={classes.title}>
            Contributors
      </Typography>
          <Hidden lgUp>
            <List className={classes.root}>
              {contributors.map((profile, i) =>
                <>
                  <ListItem alignItems="flex-start" key={i}>
                    <ListItemAvatar>
                      <Avatar className={classes.avatar} alt={"Profile picture for " + profile.alias} src={profile.profilePict} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={profile.alias}
                      secondary={
                        <React.Fragment>
                          <Typography component="span" variant="body2" className={classes.inline} color="textPrimary" >
                            {profile.fullName}
                          </Typography>
                          <br />
                          NPM : {profile.npm}
                          <br />
                          <Link href={profile.link}>{profile.link}</Link>
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                  <Divider variant="inset" component="li" key={i + "-divider"} />
                </>
              )}
            </List>
          </Hidden>
        </Grid>
        <Hidden mdDown>
          <Grid container spacing={3}>
            {contributors.map((profile, i) =>
              <Grid item xs key={i}>
                <ListItemAvatar>
                  <Avatar className={classes.avatar} alt={"Profile picture for " + profile.alias} src={profile.profilePict} />
                </ListItemAvatar>
                <div className={classes.demo}>
                  <ListItem>
                    <ListItemText
                      primary={profile.alias}
                      secondary={
                        <React.Fragment>
                          <Typography component="span" variant="body2" className={classes.inline} color="textPrimary" >
                            {profile.fullName}
                          </Typography>
                          <br />
                          NPM : {profile.npm}
                          <br />
                          <Link href={profile.link}>{profile.link}</Link>
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                </div>
              </Grid>
            )}
          </Grid>
        </Hidden>
      </Container>
    </Box>
  );
}

