import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import CourseGraph from '../../common/coursegraph/CourseGraph';
import { withStyles } from '@material-ui/core/styles';
import { Helmet } from "react-helmet";
import { withQueryParams, StringParam } from 'use-query-params';

const applyStyle = withStyles(theme => ({
  graphBg: {
    backgroundColor: theme.palette.background.default
  }
}))

class Graph extends Component {
  state = {
    network: null
  }

  componentDidMount() {
    const { userStore } = this.props;
    userStore.fetchKuliahData();
  }

  render() {
    const { userStore, classes, query } = this.props;
    const { q = "" } = query;

    const { network } = this.state;
    let dataKuliah = [];
    if (userStore && userStore.kuliahData) {
      // cari barang
      dataKuliah = userStore.kuliahData.filter((k) =>
       !q || ((k.nama.toLowerCase().includes(q.toLowerCase()) || k.kode.toLowerCase().includes(q.toLowerCase())) && ("!wajib".includes(q) ? k.wajib : true)))
        .map(e => e.kode);

      if (dataKuliah.length > 0 && dataKuliah.length !== userStore.kuliahData.length) {
        let autoFocus = setInterval(() => {
          if (network) {
            network.focus(dataKuliah[0]);

            clearInterval(autoFocus);
          }
        }, 100);
      }
    }

    return (
      <div className={classes.graphBg}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Grafik</title>
        </Helmet>
        <CourseGraph all={userStore.kuliahData} height="calc(100vh - 88px)" autoresize onNetworkReady={e => this.setState({ network: e })} special={dataKuliah} />
      </div>
    )
  }
}

export default inject("userStore")(
  withQueryParams({
    q: StringParam
  },
    applyStyle(observer(Graph))
  ));