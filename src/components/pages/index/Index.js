import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Box from '@material-ui/core/Box'
import CourseLister from '../../common/courselister/Courselister';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { withQueryParams, StringParam } from 'use-query-params';
import Link from '@material-ui/core/Link';
import { Link as LinkRouter } from "react-router-dom";
import { Helmet } from "react-helmet";

const styles = ({ palette }) => ({
  bodyBg: {
    background: palette.grey[300]
  },
  footer: {
    padding: 10,
    marginTop: 'auto',
    backgroundColor: 'white',
  },
});

class Index extends Component {
  componentDidMount() {
    const { userStore } = this.props
    userStore.fetchKuliahData()
  }
  render() {
    const { query, classes, userStore } = this.props
    const { q } = query;
    let dataKuliah = userStore.kuliahData.filter((k) =>
      !q || ((k.nama.toLowerCase().includes(q.toLowerCase()) || k.kode.toLowerCase().includes(q.toLowerCase())) && ("!wajib".includes(q) ? k.wajib : true)));

    let semesters = {};
    dataKuliah.map((data) => {
      if (!semesters.hasOwnProperty(data.semester)) {
        semesters[data.semester] = [];
      }
      return semesters[data.semester].push(data);
    })

    return (
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Beranda</title>
        </Helmet>
        <Box className={classes.bodyBg} py={5}>
          <Container>
            {Object.keys(semesters).map((semesterInfo, key) => <Box mb={10} key={"d-" + key}>
              <Typography variant="h5" display="block">
                <Box fontWeight="fontWeightLight">Semester {semesterInfo}</Box>
              </Typography>
              <CourseLister data={semesters[semesterInfo]} />
            </Box>)}
          </Container>
        </Box>
        <footer className={classes.footer}>
          <Container maxWidth="md">
            <Typography variant="body2" color="textSecondary" align="center">
              Data diambil dari
              <Link href="https://github.com/ftisunpar/data/">
                FTIS UNPAR Open Data
              </Link>{' '}
              ,untuk informasi lebih lanjut dapat mengunjungi {' '}
              <Link component={LinkRouter} to="/about">halman ini</Link>
            </Typography>
          </Container>
        </footer>
      </div>
    )
  }
}
Index.propTypes = {
}

export default inject("userStore")(
  withQueryParams({
    q: StringParam
  },
    withStyles(styles)(
      observer(Index)
    )
  )
);
