import React from 'react';
import {
  HashRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import IndexPage from "./components/pages/index/Index";
import GraphPage from "./components/pages/graph/Graph";
import AboutPage from "./components/pages/about/About";
import Appmenu from "./components/common/appmenu/Appmenu";
import UserStore from "./store/userstore";
import { QueryParamProvider } from 'use-query-params';

import { Provider } from 'mobx-react'
import { drawerWidth } from "./components/common/appmenu/consts";
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
}));

function App() {
  let classes = useStyles();

  return (
    <div className={classes.root}>
      <Provider userStore={new UserStore()}>
        <Router>
          <div className={classes.content}>
          <QueryParamProvider ReactRouterRoute={Route}>
            <Appmenu />
            <Switch>
              <Route exact path="/"><IndexPage /></Route>
              <Route exact path="/graph"><GraphPage /></Route>
              <Route exact path="/about"><AboutPage /></Route>
            </Switch>
          </QueryParamProvider>
          </div>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
