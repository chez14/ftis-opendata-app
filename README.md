# Parhunt v2

Accessible on [chez14.gitlab.io/ftis-opendata-app/](https://chez14.gitlab.io/ftis-opendata-app/). Have suggestions or better idea? [Create a issue](https://gitlab.com/chez14/ftis-opendata-app/issues).

## Proposal

Program merupakan sebuah web service yang dapat dijalankan pada browser browser
yang ada. Program dibuat menggunakan React.js sebagai framework untuk memudahkan
pembuatan halaman webnya. API yang digunakan untuk program ini adalah FTIS Open
Data (https://github.com/ftisunpar/data). Program dibuat untuk menampilkan mata
kuliah pada semester yang dipilih dan mata kuliah apa saja yang menjadi
prasyaratnya.

Kegunaan Program :

1. Menampilkan Mata Kuliah pada semua semester dalam bentuk list dan tree.
2. Menampilkan prasyarat dari mata kuliah yang dilihat atau dicari.
3. Dapat mencari mata kuliah yang diinginkan berdasarkan nama atau kode mata kuliahnya.
4. Menampilkan mata kuliah yang dikelompokan berdasarkan semester mata kuliah tersebut dapat diambil

## Masukan untuk Proposal

Ok. Catatan saya: data harus diambil melalui web service, tidak boleh dicopy ke
project.

UX harus benar-benar bagus supaya saya bisa beri nilai di sana.



## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
